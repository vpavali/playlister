#!/usr/bin/env python
# -*- coding: utf-8 -*-

import falcon
from gevent import monkey

from builder import SSPlaylistBuilder, HLSPlaylistBuilder
from storage import MongoStorage

monkey.patch_socket()

storage = MongoStorage()

ssb = SSPlaylistBuilder(storage)
hlsb = HLSPlaylistBuilder(storage)

app = falcon.API()
app.add_route('/v0/pl/ss/{dialect}/{drm}/{resource_id}.{suffix}/manifest', ssb)
app.add_route('/v0/pl/hls/{resource_id}.m3u8', hlsb)
